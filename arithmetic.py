def add(x, y):
    result = {}
    if x == 0 and y == 0:
        result['sum'] = 0
        result['carry'] = 0
    elif x == 0 and y == 1:
        result['sum'] = 1
        result['carry'] = 0
    elif x == 1 and y == 0:
        result['sum'] = 1
        result['carry'] = 0
    elif x == 1 and y == 1:
        result['sum'] = 0
        result['carry'] = 1
    else:
        result['sum'] = None
        result['carry'] = None

    return result
