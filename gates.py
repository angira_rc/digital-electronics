def and_gate(x, y):
    x = int(x)
    y = int(y)

    result = {}
    if x == 0 and y == 0:
        return 0
    elif x == 0 and y == 1:
        return 0
    elif x == 1 and y == 0:
        return 0
    elif x == 1 and y == 1:
        return 1
    else:
        return None

def or_gate(x, y):
    x = int(x)
    y = int(y)

    result = {}
    if x == 0 and y == 0:
        return 0
    elif x == 0 and y == 1:
        return 1
    elif x == 1 and y == 0:
        return 1
    elif x == 1 and y == 1:
        return 1
    else:
        return None

def xor_gate(x, y):
    x = int(x)
    y = int(y)

    result = {}
    if x == 0 and y == 0:
        return 0
    elif x == 0 and y == 1:
        return 1
    elif x == 1 and y == 0:
        return 1
    elif x == 1 and y == 1:
        return 0
    else:
        return None

def not_gate(x):
    if x:
        x = 0
    else:
        x = 1
    return x

def and_opr(a, b):
    a = str(a)
    b = str(b)

    greater = None
    lesser = None

    if len(a) > len(b):
        greater = a
        lesser = b
    else:
        greater = b
        lesser = a

    final = []
    for i in range(len(greater), 0, -1):
        # i-=1
        try:
            result = and_gate(greater[i], lesser[i])
            final.append(result)
        except IndexError:
            result = and_gate(greater[i], 0)
            final.append(result)

    final.reverse()
    final = str(final)
    print("".join(final))


# def or_opr(a, b):
#     a = str(a)
#     b = str(b)
#
#     greater = None
#     lesser = None
#
#     if len(a) > len(b):
#         greater = a
#         lesser = b
#     else:
#         greater = b
#         lesser = a
#
#     final = []
#     for i in range(len(greater), 0, -1):
#         i-=1
#         try:
#             result = or_gate(greater[i], lesser[i])
#             final.append(result)
#         except IndexError:
#             result = or_gate(greater[i], 0)
#             final.append(result)
#
#     final.reverse()
#     final = str(final)
#     print("".join(final))
#

# def xor_opr(a, b):
#     a = str(a)
#     b = str(b)
#
#     greater = None
#     lesser = None
#
#     if len(a) > len(b):
#         greater = a
#         lesser = b
#     else:
#         greater = b
#         lesser = a
#
#     final = []
#     for i in range(len(greater), 0, -1):
#         i-=1
#         try:
#             result = xor_gate(greater[i], lesser[i])
#             final.append(result)
#         except IndexError:
#             result = xor_gate(greater[i], 0)
#             final.append(result)
#
#     final.reverse()
#     final = str(final)
#     print("".join(final))
#

# def nand_opr(a, b):
#     a = str(a)
#     b = str(b)
#
#     greater = None
#     lesser = None
#
#     if len(a) > len(b):
#         greater = a
#         lesser = b
#     else:
#         greater = b
#         lesser = a
#
#     final = []
#     for i in range(len(greater), 0, -1):
#         i-=1
#         try:
#             result = and_gate(greater[i], lesser[i])
#             if result != None:
#                 result = not_gate(result)
#             final.append(result)
#         except IndexError:
#             result = and_gate(greater[i], 0)
#             if result != None:
#                 result = not_gate(result)
#             final.append(result)
#
#     final.reverse()
#     final = str(final)
#     print("".join(final))

def and_block(greater, lesser):
    result = 0
    try:
        result = and_gate(greater, lesser)
    except IndexError:
        result = and_gate(greater, 0)
    return result

def or_block(greater, lesser):
    result = 0
    try:
        result = or_gate(greater, lesser)
    except IndexError:
        result = and_gate(greater, 0)
    return result

def xor_block(greater, lesser):
    result = 0
    try:
        result = xor_gate(greater, lesser)
    except IndexError:
        result = xor_gate(greater, 0)
    return result

def nor_block(greater, lesser):
    result = 0
    try:
        result = and_gate(greater, lesser)
        if result != None:
            result = not_gate(result)
    except IndexError:
        result = and_gate(greater, 0)
        if result != None:
            result = not_gate(result)
    return result

def nand_block(greater, lesser):
    result = 0
    try:
        result = and_gate(greater, lesser)
        if result != None:
            result = not_gate(result)
    except IndexError:
        result = and_gate(greater, 0)
        if result != None:
            result = not_gate(result)
    return result

def univ(opr, a, b):
    a = str(a)
    b = str(b)

    greater = None
    lesser = None

    if len(a) > len(b):
        greater = a
        lesser = b
    else:
        greater = b
        lesser = a

    final = []
    for i in range(len(greater), 0, -1):
        i-=1

        if opr == 'and':
            result = and_block(greater[i], lesser[i])
        elif opr == 'or':
            result = or_block(greater[i], lesser[i])
        elif opr == 'xor':
            result = xor_block(greater[i], lesser[i])
        elif opr == 'nand':
            result = nand_block(greater[i], lesser[i])

        final.append(result)

    final.reverse()
    final = str(final)
    print("".join(final))
